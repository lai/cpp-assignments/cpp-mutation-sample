#ifndef BYTEARRAY_H
#define BYTEARRAY_H
#include <stddef.h>

typedef struct {
	char *bytes;
	size_t size;
	size_t count;
} ByteArray;

#ifdef __cplusplus
extern "C" {
#endif

int bytearray_init(ByteArray *self, const char *string);

int bytearray_dtor(ByteArray *self);


int bytearray_join(ByteArray *self, ByteArray vector[], int size);
int bytearray_join_from_char(ByteArray *, const char **, int size);


int bytearray_init_from_vector(ByteArray bytes[], const char **vector, int size);
int bytearray_dtor_from_vector(ByteArray bytes[], int size);


int bytearray_compare_with_bytes(ByteArray *self, const char *string);
int bytearray_compare_with_vector(ByteArray bytes[], const char **vector, int size);



#ifdef __cplusplus
}
#endif
#endif
