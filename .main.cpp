#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "clawesome.h"

int main()
{
    
    POS pos = {0, 1};
    MUTABLE_BOARD board;
    ByteArray claw[BOARD_SIZE]; 
    bytearray_init_from_vector(claw, C_OPEN, 3);
    MUTABLE_ITEMS items = ITEMS_STATIC_INIT;
    while (1) {
        
        BOARD_INIT(board);
        
        // Get first character of each line
        int c = getchar();
        while (getchar() != '\n')
            continue;
       
        clear_screen(); 
        render_frame(board, pos, 0, items, "  ");
        for (int i = 0; i < BOARD_SIZE; i++) {
            puts(board[i].bytes);
        }
        BOARD_DTOR(board);

        switch (c) {
            case 'a':
                pos[1] = (pos[1] - 1 >= 0) ? --pos[1]: pos[1];
                break;
            case 'd': 
                pos[1] ++;
                break;
            case 's':
                    pos[1] --;
                    do_grab(board, pos, items); 
                    break;
            case 'q':
                    break;
            case '\n':
                    break;
        
        }     
    }
}
