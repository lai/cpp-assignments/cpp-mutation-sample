from revChatGPT.ChatGPT import Chatbot
import glob
import difflib
import os

# go to https://chat.openai.com/api/auth/session and paste your session token here:
sessionToken = ""

# Each source file has an intended purpose, which should be given to ChatGPT
# so it can attempt to determine what went wrong with the mutated program.
# edit this dict to add more source files (generate the mutations first!)
purposes = {
    "fib.cpp"       : "calculate the fibonacci series.",
    "clawesome.cpp" : "simulate a claw machine.",
    "bytearray.cpp" : "define functions that relate to and operate on byte arrays.",
    "triangle.cpp"  : "define functions that relate to and operate on triangles."
}

# store data related to each prompt
class GPTPrompt:
    originalFileName = ""
    mutatedFileName = ""

    def __init__(self, originalFileName, mutatedFileName):
        self.originalFileName = originalFileName
        self.mutatedFileName  = mutatedFileName

    # build the question string to ask ChatGPT
    def getQuestion(self):
        if not self.originalFileName in purposes:
            raise Exception("You need to specify a purpose for " + self.originalFileName + " so that ChatGPT can attempt to find errors!")
        output  = "The following C plus plus implementation file is part of a program that is supposed to "
        output += purposes[self.originalFileName] + "\n"
        output += "What, if anything, is wrong with the code?" + "\n\n"
        output += open(self.originalFileName,"r").read()
        return output
    
    # get the diff between the original and mutated files
    def getDiff(self):
        output = ""
        diff = difflib.unified_diff(open(self.originalFileName,"r").read(), open(self.mutatedFileName,"r").read())
        for outputLine in diff:
            output += outputLine
        return output

prompts = list()

# add prompts for all mutations of a specific source file
def addSourceFileToPrompts(filename):
    for mutatedFile in glob.glob("unittests/mut/*" + filename):
        justFileName = mutatedFile.split('/')[-1]
        # if a response has already been generated, skip this mutation
        if not os.path.isfile("unittests/GPT_responses/" + justFileName + ".log"):
            prompts.append(GPTPrompt(filename, mutatedFile))
        else:
            print("Skipping file: " + mutatedFile)

# use source files and their mutations to create the list of prompts
for sourceFile in purposes.keys():
    addSourceFileToPrompts(sourceFile)

chatbot = Chatbot({
    "session_token" : sessionToken
}, conversation_id=None, parent_id=None)

# get responses 
for currentPrompt in prompts:
    question = currentPrompt.getQuestion()
    response = chatbot.ask(currentPrompt.getQuestion(), conversation_id=None, parent_id=None)['message']

    newFile = "unittests/GPT_responses/" + currentPrompt.mutatedFileName.split("/")[-1] + ".log"

    # write diffs and responses for comparison to the unittests/GPT_responses directory
    with open(newFile, "w") as outfile:
        outfile.write(currentPrompt.getDiff())
        outfile.write(response)
        print("response to " + currentPrompt.mutatedFileName + " written!")