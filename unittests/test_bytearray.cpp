#include <string.h>
#include "catch.hpp"
#include "mutation.h"

#include "../bytearray.h"

TEST_CASE("test join")
{
	MUTATION_START(bytearray_join, "bytearray.so")
	{
		typedef int (*JoinFunc)(ByteArray *, ByteArray [], int);
		JoinFunc testfunc = (JoinFunc) test_function();

		ByteArray join;
		bytearray_init(&join, " ");
		
		ByteArray vector[2];
		bytearray_init(vector, "Hello");
		bytearray_init(vector + 1, "World");

		testfunc(&join, vector, 2);
			
		REQUIRE(strcmp(join.bytes, "Hello World") == 0);		

	}
	MUTATION_END
}


TEST_CASE("test compare")
{
    const char *a1 = "Hello, World!";
    ByteArray b1 = { "Hello, World!", 12, 12};
    
    const char *a2 = "Hello World!";
        
    MUTATION_START(bytearray_compare_with_bytes, "bytearray.so") {

        typedef int (*TestFunc)(ByteArray *, const char *);
        TestFunc func = (TestFunc) test_function();
        REQUIRE(func(& b1, a1));
        REQUIRE(! func(& b1, a2));

    } MUTATION_END;
    
}
