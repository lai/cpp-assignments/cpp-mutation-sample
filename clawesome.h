#ifndef CLAWESOME_H
#define CLAWESOME_H

#ifdef __cplusplus
extern "C" {
#endif
#include "bytearray.h"

/* These are the prizes */
#define NOTHING 	" "
#define ACORN 		"{)"
#define HEADPHONES 	"db"
#define LOLLIPOP	"O-"
#define HEART		"<3"
#define MAGIC_WAND	"+-"
#define BALL		"()"

#define ITEMS_STATIC_INIT { ACORN, HEADPHONES, LOLLIPOP, HEART, MAGIC_WAND, BALL  }

typedef int POS[2];
typedef ByteArray CLAW[5];

extern const POS C_START;

#define MAX_ITEMS 	6
#define BOARD_SIZE 	7

//  The order in which items are placed
//  in the claw machine at the start of the game
extern const char * const ITEMS_START[6];

// Mutable array
typedef const char *MUTABLE_ITEMS[sizeof(ITEMS_START)/sizeof(char *)];

extern const char * C_CLOSED[3];
extern const char * C_OPEN[3];

int CLAW_template_init(CLAW claw, const char **tmplate, int size);
int CLAW_dtor(CLAW claw);

extern const char * BOARD[BOARD_SIZE];

typedef ByteArray MUTABLE_BOARD[sizeof(BOARD) / sizeof(const char*)];

void clear_screen(void);
void frame_sleep(void);

#define BOARD_INIT(board)   (bytearray_init_from_vector(board, BOARD, BOARD_SIZE))
#define BOARD_DTOR(board)   (bytearray_dtor_from_vector(board, BOARD_SIZE))


/*
 * @brief Copy item into claw
 * */
int claw_with_item(CLAW claw_vector, int vec_size, const char *item);
/*
 * @brief Drop the claw
 * @return size of new claw */
int extend_claw(CLAW claw_vector, int vec_size, POS claw_pos);

int copy_objects(const MUTABLE_ITEMS items, MUTABLE_BOARD board);

int copy_claw(CLAW claw_vector, int vec_size, POS claw_pos, MUTABLE_BOARD board);

const char *item_at_claw_pos(POS pos, MUTABLE_ITEMS items);

int do_grab(MUTABLE_BOARD board, POS claw_pos, MUTABLE_ITEMS items);


int render_frame(MUTABLE_BOARD board, POS claw_pos, int claw_closed, const char **items, const char *grabbed);
#ifdef __cplusplus
}
#endif
#endif
