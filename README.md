# 0 - unittests & doxygen

This repo demonstrates a simple framework for unit testing and documentation.
We will go over the repo dependencies, how it works, its features, and how to
use it.

# Dependencies

```bash
sudo apt install gcov       # Generate line data
sudo apt install gcovr      # Report generator for gcovr
sudo apt install doxygen    # Documentation generator
```

## How does it work?
In the root directory, there is a makefile and a unittests directory. Invoking

```
make
```

This compiles all .cpp files into .o files twice. The first compilation simply builds
the file with the specified flags. The second time builds with the `--coverage`
flag and places it in the directory.

```
make test
```

This compiles everything `make` does and it runs the test suite.

## Unit Testing Features

Invoking `make test` does 4 things

1. Unit testing - This repo uses catch.hpp to run unit testing. To read more about
catch.hpp, go here.

2. Integrated memory checking with valgrind - Unit tests are run using valgrind
and a leak summary is shown afterwards.

3. Collecting coverage and branch info - Because the unit test is compiled with
`--coverage` flag, when the test is executed, coverage data is generated.

4. Reporting - Taking data generated from executing the unit test, the `gcovr`
binary creates html report and places it in `./unittests/coverage`

5. Mutation testing - mutated source code is placed in `./unittests/mut`
Each mutated source file is compiled to a shared object library and placed in `./unittests/lib`

## Doxygen

Invoking doxygen with the given Doxyfile creates a doc folder

```
doxygen
```

# How to use it?

1. SSH into S&T Linux machines with the following command. Unfortunately, the S&T Linux machine does not currently have `gcovr` installed

```bash
# Replace <user> with SSO and <number> with a zero-padded, two digit number in range [01, 30] inclusive
ssh -L 8000:localhost:8000 <user>@rc<number>xcs213.managed.mst.edu
``` 

2. `git clone` and `cd` into this repo. If you want to view documentation, run `python3 -m http.server`. Then open up a browser and navigate to https://localhost:8000
3. Invoke `make`

4. From here you can either run standard unit testing (described above) or mutation testing

## Mutation testing
Mutation testing requires the pycparser package, installed from pip

```bash
pip3 install -U pycparser
```

5. To run mutation tests, make sure to invoke `make` first
6. Then run `make mutate`

### Writing Mutation Unit Tests
pycparser only parses c99 code. Not C++. So the source file that contains the tested function must be C99 compatible.
The testing code can be written in C++ code.
See `./unittests/test_triangle.cpp` for an example. At a minimum, the following headers must be included
```C
#include "catch.hpp" // Unit testing framework
#include "mutation.h" // Macros that set up mutation context
```
In triangle.cpp, the function `triangle_tritype` is defined. The mutated variant
is placed in a shared-object file with a prefix + `triangle.so`.

```C
TEST_CASE("<test case name>")
{
  /**
  Start mutation context 
    <test_function> is the symbol to be mutated,
    <shared object> is name of the translation unit with .so extension 
  */
  MUTATION_START(<test_function>, "<shared object>.so")
  {
    typedef int (test_func_type)(*functype)(int); // Typedef a function pointer
    test_func_type mutated = (functype) test_function(); // Retrieve the mutated function
    REQUIRE(test_func(1) == 0); // Do tests
  }
  /** END of Mutation context */
  MUTATION_END
}
```

The original, unmutated function can always be accessed because it is
statically linked.


